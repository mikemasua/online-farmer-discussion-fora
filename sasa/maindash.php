<?php
session_start();
require 'db.php';
include("functions/functions.php");

if(!isset($_SESSION['user_email'])){
	header("location: index.php");
	}else{
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content=" Online Clearance Dashboard">

<title>Farmers Chat Forum</title>

    <!-- Bootstrap core CSS -->
<link href="assets/css/bootstrap.css" rel="stylesheet" />
<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet"/>
    <!--external css-->
<link href="assets/css/zabuto_calendar.css" rel="stylesheet"/>
<link rel="stylesheet"/>
<link href="assets/js/gritter/css/jquery.gritter.css" rel="stylesheet"/>
<link href="assets/lineicons/style.css" rel="stylesheet"/>
<link rel="shortcut icon" href="images/favicon.png" />

<!-- Custom styles for this template -->
<link href="assets/css/style.css" rel="stylesheet"/>
<link href="assets/css/style-responsive.css" rel="stylesheet"/>
<link href="assets/css/zabuto_calendar.css" rel="stylesheet"/>
<script src="assets/js/chart-master/Chart.js"></script>
    
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<section id="container" >
<!-- ******** TOP BAR CONTENT & NOTIFICATIONS ************************ -->
 <!--header start-->
<header class="header black-bg">
<div class="sidebar-toggle-box">
<div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
</div>
<!--logo start-->
<a href="#" class="logo" style="color: #43b1a9;"><b>Farmers Forum</b></a> 
<!--logo end-->
<div class="nav notify-row" id="top_menu">
<!--  notification start -->
<ul class="nav top-menu">

<!-- settings start -->
<li>
<a href="maindash.php"> Home</a>
</li>

<li class="#">
<a href="members1.php"> Members</a>
</li>
<li class="#">
<strong> Topics:</strong>
<?php
$get_topics="select * from topics";
$run_topics=mysqli_query($con,$get_topics);
while ($row=mysqli_fetch_array($run_topics))
{
$topic_id=$row['topic_id'];
$topic_title=$row['topic_title'];
echo "<li><a href= 'topic1.php?topic=$topic_id'>$topic_title </a></li>" ;
}?>
</a>
</li>

<li class="#">
<form action="# " method="get" id="form1">
<input type="text"  name="user_query" placeholder="search a topic" autofocus id="name2" required="required" id="inputSuccess"/>

<button  type="submit" name ="search" class="btn btn-success "><i class="glyphicon glyphicon-search"></i></button>

</form>
</a>
</li>
</header>
      <!--header end-->
 
<!-- ********************* MAIN SIDEBAR MENU ************************ -->
      <!--sidebar start-->
<aside>
<div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
<ul class="sidebar-menu" id="nav-accordion">
<h3 class="centered">User Profile</h3>           
 
<?php

 $user= $_SESSION['user_email'];
 $get_user= "select * from users where user_email='$user' ";
 $run_user= mysqli_query($con,$get_user);
 $row=mysqli_fetch_array($run_user);
 
 $user_id= $row['user_id'];
 $user_name= $row['user_name'];
 $user_image= $row['user_image'];
 $user_county= $row['user_county'];
 $user_phone= $row['user_phone'];
 $last_login= $row['last_login'];
 
 $user_posts="select * from posts where user_id='$user_id'";
 $run_posts= mysqli_query($con,$user_posts);
 $posts= mysqli_num_rows($run_posts); 
 
 // getting the number of unread messages
 
 $select_msg ="select * from messages  where receiver = '$user_id' AND status='unread' ORDER BY 1 DESC";
$run_msg =mysqli_query($con,$select_msg);
$count_msg= mysqli_num_rows($run_msg);


echo " 
<p class='centered'><a href='#'><img src='images/$user_image' class='img-circle' width='80' height='80'></a></p>
<h5 class='centered'></h5>

<li class='mt'>
<a class='active' href='maindash.php#'>
<i class='fa fa-dashboard'></i>
<span>Dashboard</span>
 </a>
</li>

<li class='sub-menu'>
<a href='javascript:;' >
<i class='fa fa-user'></i>
<span><strong> Full Name (s): </strong> $user_name </span>
</a>
</li>

<li class='sub-menu'>
<a href='javascript:;' >
<i class='fa fa-phone'></i>
<span><strong> County:  </strong> $user_county  </span>
</a>
</li>
<li class='sub-menu'>
<a href='messages1.php?inbox&u_id=$user_id' ><i class='fa fa-envelope'></i> My SmS ($count_msg)</a> 
</li>

<li class='sub-menu'>
<a href='my_posts1.php?u_id=$user_id'> <i class=' fa fa-pencil-square-o'></i> My Posts ($posts)</a>
</li>

<li class='sub-menu'>
<a href='edit1.php?u_id=$user_id'><i class='fa fa-user'></i> Edit Profile</a>
</li>

<li class='sub-menu'>
<a href='javascript:;' >
<i class='fa fa-bookmark'></i>
<span>Seen $last_login </span>
</a>
</li>

<li class='sub-menu'>
<a href='logout.php'><i class='fa fa-sign-out'></i> Logout</a>
</a>
</li>

";
 ?>           
</div>
</aside>

<!-- **************  MAIN CONTENT ************************* -->
<!--main content start-->
<section id="main-content">
<section class="wrapper site-min-height">
 
<div class='col-lg-12 col-md-12 col-sm-12'>
<h3 class='alert alert-success'>Farmers Chat Forum </h3>
<p style="flaot:right;"> Logged In as:<?php  echo $_SESSION['user_email']; ?></p>

</div>


<div class="row mt">
<div class='col-lg-8 col-md-8 col-sm-12'>

<form class="form-horizontal style-form" action="maindash.php?id= <?php echo $user_id;?>" method="post">

<div class="form-panel">
<h4 class="alert alert-success"> Post your Query,Solution,idea Here: </h4>
<div class="form-group has-success">
<div class="col-xs-10">
<input type="text" class="form-control" name="title" placeholder="Write  a Title..... " autofocus id="name2" required="required" id="inputSuccess"/>
</div>
</div>

<div class="form-group has-success">
<div class="col-xs-10">
<textarea name="content" class="form-control" placeholder ="Write a Description...." required id="inputSuccess" ></textarea>
</div>
</div>

<div class="form-group has-success">
<div class="col-xs-4">
<select class="form-control" name="topic" id="inputSuccess"> 
<option> ...Select Topic ...</option>
<?php getTopics();?>
</select>
</div>

<div class="col-xs-6">
<input type="submit" class="form-control"  name="sub" value="Post To Timeline" autofocus id="name2" required="required" id="inputSuccess"/>
</div>
</div>
</div>

</form>
<div class='col-lg-12 col-md-12 col-sm-12'>

<h3 class='alert alert-success'>Latest Posts </h3>
</div>

<?php insertPost();?>
<?php get_posts();?>

 <div class="col-lg-3 ds">
 
  <!-- USERS ONLINE SECTION -->

<h3  style='background: rgba(19, 35, 47, 0.9); '>TEAM MEMBERS</h3>
 <?php
getUsers();
?>                  
      <!-- CALENDAR-->
<div id="calendar" class="mb">
<div class="panel green-panel no-margin">
<div class="panel-body">
<div id="date-popover" class="popover top" style="cursor: pointer; disadding: block; margin-left: 33%; margin-top: -50px; width: 175px;">
<div class="arrow"></div>
<h3 class="popover-title" style="disadding: none;"></h3>
<div id="date-popover-content" class="popover-content"></div>
 </div>
 <div id="my-calendar"></div>
</div>
</div>
 </div><!-- / calendar -->
</div><!-- /col-lg-3 -->
</div>
</div>
</div><!-- /col-lg-6 -->
</div><!-- /col-lg-3 -->
              </div><! --/row -->
    
    		
</section><! --/wrapper -->
</section><!-- /MAIN CONTENT -->
<!--main content end-->

</section>

    <!-- js placed at the end of the document so the pages load faster -->
	<script src="assets/js/fancybox/jquery.fancybox.js"></script>    
    <script src="assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>


    <!--common script for all pages-->
    <script src="assets/js/common-scripts.js"></script>

    <!--script for this page-->
  
  <script type="text/javascript">
      $(function() {
        //    fancybox
          jQuery(".fancybox").fancybox();
      });

  </script>
  
  <script>
      //custom select box

      $(function(){
          $("select.styled").customSelect();
      });

  </script>

  <!--main content end-->
<!--footer start-->
<!-- js placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/jquery-1.8.3.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="assets/js/jquery.sparkline.js"></script>

 <!--common script for all pages-->
    <script src="assets/js/common-scripts.js"></script>
    
    <script type="text/javascript" src="assets/js/gritter/js/jquery.gritter.js"></script>
    <script type="text/javascript" src="assets/js/gritter-conf.js"></script>

    <!--script for this page-->
    <script src="assets/js/sparkline-chart.js"></script>    
	<script src="assets/js/zabuto_calendar.js"></script>	
	
	<script type="text/javascript">
        $(document).ready(function () {
        var unique_id = $.gritter.add({
            // (string | mandatory) the heading of the notification
            title: 'Welcome <?php  echo $_SESSION['fname'];?> to Online Clearance!',
            // (string | mandatory) the text inside the notification
            text: 'Feel free to use this system. The system is in beta stage.',
            // (string | optional) the image to display on the left
            image: 'assets/img/elite.jpg',
            // (bool | optional) if you want it to fade out on its own or just sit there
            sticky: true,
            // (int | optional) the time you want it to be alive for before fading out
            time: '',
            // (string | optional) the class name you want to apply to that specific message
            class_name: 'my-sticky-class'
        });

        return false;
        });
	</script>
	<script type="application/javascript">
        $(document).ready(function () {
            $("#date-popover").popover({html: true, trigger: "manual"});
            $("#date-popover").hide();
            $("#date-popover").click(function (e) {
                $(this).hide();
            });
        
            $("#my-calendar").zabuto_calendar({
                action: function () {
                    return myDateFunction(this.id, false);
                },
                action_nav: function () {
                    return myNavFunction(this.id);
                },
                ajax: {
                    url: "show_data.php?action=1",
                    modal: true
                },
                legend: [
                    {type: "text", label: "Special event", badge: "00"},
                    {type: "block", label: "Regular event", }
                ]
            });
        });
        
        
        function myNavFunction(id) {
            $("#date-popover").hide();
            var nav = $("#" + id).data("navigation");
            var to = $("#" + id).data("to");
            console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
        }
    </script>
</body>
</html>
 <?php  } ?>