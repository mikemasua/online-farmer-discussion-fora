<?php
session_start();
require 'db.php';
include("functions/functions.php");

if(!isset($_SESSION['user_email'])){
	header("location: index.php");
	}else{
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content=" Online Clearance Dashboard">

   <title>Farmers Chat Forum</title>

    <!-- Bootstrap core CSS -->
<link href="assets/css/bootstrap.css" rel="stylesheet" />
<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet"/>
    <!--external css-->
<link href="assets/css/zabuto_calendar.css" rel="stylesheet"/>
<link rel="stylesheet"/>
<link href="assets/js/gritter/css/jquery.gritter.css" rel="stylesheet"/>
<link href="assets/lineicons/style.css" rel="stylesheet"/>

        
      
    
    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet"/>
   
    <link href="assets/css/style-responsive.css" rel="stylesheet"/>
    <link href="assets/css/zabuto_calendar.css" rel="stylesheet"/>
    <script src="assets/js/chart-master/Chart.js"></script>
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
  <style></style>
<section id="container" >
<!-- ******** TOP BAR CONTENT & NOTIFICATIONS ************************ -->
 <!--header start-->
<header class="header black-bg">
<div class="sidebar-toggle-box">
<div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
</div>
<!--logo start-->
<a href="#" class="logo" style="color: #43b1a9;"><b>Farmers Forum</b></a> 
<!--logo end-->
<div class="nav notify-row" id="top_menu">
<!--  notification start -->
<ul class="nav top-menu">

<!-- settings start -->
<li>
<a href="maindash.php"> Home</a>
</li>

<li class="#">
<a href="members1.php"> Members</a>
</li>
<li class="#">
<strong> Topics:</strong>
<?php
$get_topics="select * from topics";
$run_topics=mysqli_query($con,$get_topics);
while ($row=mysqli_fetch_array($run_topics))
{
$topic_id=$row['topic_id'];
$topic_title=$row['topic_title'];
echo "<li><a href= 'topic1.php?topic=$topic_id'>$topic_title </a></li>" ;
}?>
</a>
</li>

<li class="#">
<form action="# " method="get" id="form1">
<input type="text" name="user_query" placeholder="search a topic" required="required" />

<input type="submit" name ="search"  value="Search" style="background-color: #4CAF50; border: 2px solid #4CAF50;" />
</form>
</a>
</li>
</header>
      <!--header end-->
 
<!-- ********************* MAIN SIDEBAR MENU ************************ -->
      <!--sidebar start-->
<aside>
<div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
<ul class="sidebar-menu" id="nav-accordion">
<h3 class="centered">User Profile</h3>           


<?php

 $user= $_SESSION['user_email'];
 $get_user= "select * from users where user_email='$user' ";
 $run_user= mysqli_query($con,$get_user);
 $row=mysqli_fetch_array($run_user);
 
 $user_id= $row['user_id'];
 $user_name= $row['user_name'];
 $user_image= $row['user_image'];
 $user_county= $row['user_county'];
 $user_phone= $row['user_phone'];
 $last_login= $row['last_login'];
 
 $user_posts="select * from posts where user_id='$user_id'";
 $run_posts= mysqli_query($con,$user_posts);
 $posts= mysqli_num_rows($run_posts); 
 
 // getting the number of unread messages
 
 $select_msg ="select * from messages  where receiver = '$user_id' AND status='unread' ORDER BY 1 DESC";
$run_msg =mysqli_query($con,$select_msg);
$count_msg= mysqli_num_rows($run_msg);


echo " 
<p class='centered'><a href='#'><img src='images/$user_image' class='img-circle' width='80' height='80'></a></p>
<h5 class='centered'></h5>

<li class='mt'>
<a class='active' href='maindash.php#'>
<i class='fa fa-dashboard'></i>
<span>Dashboard</span>
 </a>
</li>

<li class='sub-menu'>
<a href='javascript:;' >
<i class='fa fa-user'></i>
<span><strong> Full Name (s): </strong> $user_name </span>
</a>
</li>

<li class='sub-menu'>
<a href='javascript:;' >
<i class='fa fa-phone'></i>
<span><strong> County:  </strong> $user_county  </span>
</a>
</li>
<li class='sub-menu'>
<a href='messages1.php?inbox&u_id=$user_id' ><i class='fa fa-envelope'></i> My SmS ($count_msg)</a> 
</li>

<li class='sub-menu'>
<a href='my_posts1.php?u_id=$user_id'> <i class=' fa fa-pencil-square-o'></i> My Posts ($posts)</a>
</li>

<li class='sub-menu'>
<a href='edit1.php?u_id=$user_id'><i class='fa fa-user'></i> Edit Profile</a>
</li>

<li class='sub-menu'>
<a href='javascript:;' >
<i class='fa fa-bookmark'></i>
<span>Seen $last_login </span>
</a>
</li>

<li class='sub-menu'>
<a href='logout.php'><i class='fa fa-sign-out'></i> Logout</a>
</a>
</li>

";
 ?>           
</div>
</aside>

<!-- **************  MAIN CONTENT ************************* -->
<!--main content start-->
<section id="main-content">
<section class="wrapper site-min-height">
<div class='col-lg-8 col-md-8 col-sm-12'>
<h3 class='alert alert-success'>Farmers Chat Forum </h3>
</div>



<div class="row mt">
<div class='col-lg-8 col-md-8 col-sm-12'>

<?php
 $user= $_SESSION['user_email'];
 $get_user= "select * from users where user_email='$user' ";
 $run_user= mysqli_query($con,$get_user);
 $row=mysqli_fetch_array($run_user);
 
 $user_id= $row['user_id'];
 $user_name= $row['user_name'];
 $user_image= $row['user_image'];
 $user_county= $row['user_county'];
 $user_pass= $row['user_pass'];
 $user_email= $row['user_email'];
 $user_gender= $row['user_gender'];
 $user_phone= $row['user_phone'];
 $last_login= $row['last_login'];

 
 $user_posts="select * from posts where user_id='$user_id'";
 $run_posts= mysqli_query($con,$user_posts);
 $posts= mysqli_num_rows($run_posts); 
 
 // getting the number of unread messages
 
 $select_msg ="select * from messages  where receiver = '$user_id' AND status='unread' ORDER BY 1 DESC";
$run_msg =mysqli_query($con,$select_msg);
$count_msg= mysqli_num_rows($run_msg);
 

 ?>
 </div>
 
 </div>

<div class="row mt">
<div class='col-lg-8 col-md-8 col-sm-12'>

<form class="form-horizontal style-form" action="" method="post" enctype="multipart/form-data">

<div class="form-panel">
<h4 class="alert alert-success"> Edit User Info: </h4>
<div class="form-group has-success">
<div class="col-xs-8">
<input type="text" class="form-control" name="u_name"  value="<?php echo $user_name;?>" autofocus id="name2" required="required" id="inputSuccess"/>
</div>
</div>


<div class="form-group has-success">
<div class="col-xs-4">
<input type="email" class="form-control" name="u_email"   value="<?php echo $user_email;?>" autofocus id="name2" required="required" id="inputSuccess"/>
</div>

<div class="col-xs-4">
<input  class="form-control" type="password" name="u_pass" value="<?php echo $user_pass;?>" autofocus id="name2" required="required" id="inputSuccess"/>
</div>
</div>


<div class="form-group has-success">
<div class="col-xs-4">
<input  class="form-control" type="text" name="u_phone" value="<?php echo $user_phone;?>" autofocus id="name2" required="required" id="inputSuccess"/>
</div>

<div class="col-xs-4">
<select class="form-control" name="u_gender"  id="inputSuccess"> 
<option> <?php echo $user_gender;?></option>
    <option> Male</option>
    <option> Female</option>
</select>
</div>
</div>


<div class="form-group has-success">
<div class="col-xs-4">
<select  class="form-control" id="inputSuccess" name="u_role" > 

  <option value="">......Select Category.....</option>
  <option value="Vet.Officer"> Vet.Officer</option>
  <option value ="Farmer"> Farmer</option>
  <option value ="Agronomist"> Agronomist</option>
  <option value="Agri.Engineer"> Agri.Engineer</option>
  <option value="Water.Engineer"> Water.Engineer</option>
  <option value="Laboaratoligist">Laboaratoligist</option>
  <option value="HumanResource"> HumanResource</option>
  </select>
</div>

<div class="col-xs-4">
<select class="form-control" name="u_county"  id="inputSuccess"> 
 <option value="">......Select county.....</option>
    <option>Kitui</option>
      <option>Nrb</option>
      <option>Msa</option>
      <option>Kajiado</option>
      <option>Machakos</option>
      <option>Kisii</option>
      <option>Embu</option>
      <option>Eldoret</option>
      <option>Meru</option>
      <option>Busia</option>
      <option>Kwale</option>
      <option>Kiambu</option>
      <option>Makueni</option>
      <option>kisumu</option>
      <option>Kitale</option>
      <option>Siaya</option>
      </select>
</select>
</div>
</div>

<div class="form-group has-success">
<div class="col-xs-8">
<input type="file"  class="form-control" name="u_image" value="<?php echo $user_image;?>"  required="required" id="inputSuccess"/>
</div>
</div>
<button class="btn btn-success " name="update" type="submit"><i class="glyphicon glyphicon-send"></i> Update</button>
</div>
</div>

</form>
<?php
 if(isset($_POST['update'])){
	$u_name= $_POST['u_name'];
	$u_email=$_POST['u_email'];
	$u_pass=$_POST['u_pass'];
	$u_phone=$_POST['u_phone'];
	$u_role=$_POST['u_role'];
	$u_county=$_POST['u_county'];
	$u_image=$_FILES['u_image']['name'];
	$image_tmp=$_FILES['u_image']['tmp_name'];
	
	move_uploaded_file($image_tmp,"images/$u_image");
	
	
	
	$update = "update users set user_name ='$u_name',user_email ='$u_email',user_pass ='$u_pass',user_phone ='$u_phone',     user_role ='$u_role',user_county ='$u_county',user_image ='$u_image'   where user_id='$user_id' ";
	
	$run =mysqli_query($con,$update);
	
	if($run){
	 echo "<script>alert('Your Profile Updated sucessfully')</script>";	
	 echo "<script>window.open('maindash.php','_self')</script>";	
		
		}
}
  ?> 

 </form>
 </div>
</div><!-- /col-lg-3 -->
</div><! --/row -->
    
    		
</section><! --/wrapper -->
</section><!-- /MAIN CONTENT -->
<!--main content end-->

</section>

    <!-- js placed at the end of the document so the pages load faster -->
	<script src="assets/js/fancybox/jquery.fancybox.js"></script>    
    <script src="assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>


    <!--common script for all pages-->
    <script src="assets/js/common-scripts.js"></script>

    <!--script for this page-->
  
  <script type="text/javascript">
      $(function() {
        //    fancybox
          jQuery(".fancybox").fancybox();
      });

  </script>
  
  <script>
      //custom select box

      $(function(){
          $("select.styled").customSelect();
      });

  </script>

  <!--main content end-->
<!--footer start-->
<!-- js placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/jquery-1.8.3.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="assets/js/jquery.sparkline.js"></script>

 <!--common script for all pages-->
    <script src="assets/js/common-scripts.js"></script>
    
    <script type="text/javascript" src="assets/js/gritter/js/jquery.gritter.js"></script>
    <script type="text/javascript" src="assets/js/gritter-conf.js"></script>

    <!--script for this page-->
    <script src="assets/js/sparkline-chart.js"></script>    
	<script src="assets/js/zabuto_calendar.js"></script>	
	
	<script type="text/javascript">
        $(document).ready(function () {
        var unique_id = $.gritter.add({
            // (string | mandatory) the heading of the notification
            title: 'Welcome <?php  echo $_SESSION['fname'];?> to Online Clearance!',
            // (string | mandatory) the text inside the notification
            text: 'Feel free to use this system. The system is in beta stage.',
            // (string | optional) the image to display on the left
            image: 'assets/img/elite.jpg',
            // (bool | optional) if you want it to fade out on its own or just sit there
            sticky: true,
            // (int | optional) the time you want it to be alive for before fading out
            time: '',
            // (string | optional) the class name you want to apply to that specific message
            class_name: 'my-sticky-class'
        });

        return false;
        });
	</script>
	<script type="application/javascript">
        $(document).ready(function () {
            $("#date-popover").popover({html: true, trigger: "manual"});
            $("#date-popover").hide();
            $("#date-popover").click(function (e) {
                $(this).hide();
            });
        
            $("#my-calendar").zabuto_calendar({
                action: function () {
                    return myDateFunction(this.id, false);
                },
                action_nav: function () {
                    return myNavFunction(this.id);
                },
                ajax: {
                    url: "show_data.php?action=1",
                    modal: true
                },
                legend: [
                    {type: "text", label: "Special event", badge: "00"},
                    {type: "block", label: "Regular event", }
                ]
            });
        });
        
        
        function myNavFunction(id) {
            $("#date-popover").hide();
            var nav = $("#" + id).data("navigation");
            var to = $("#" + id).data("to");
            console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
        }
    </script>
</body>
</html>
 <?php  } ?>