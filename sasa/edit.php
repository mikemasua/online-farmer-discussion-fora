<!doctype html>
<?php
session_start();
include("includes/connection.php");
include("functions/functions.php");

if(!isset($_SESSION['user_email'])){
	header("location: index.php");
	}else{
?>
<html>
<head>
<meta charset="utf-8">

<link rel="stylesheet" href="styles/home_style5.css" media="all" />
<style> input[type='file']{width:190px;} </style>

<title>Farmers ChatBox</title>
</head>
<body>
<div class="container">
<div id="head_wrap">
<div id="header">
<h4 > Welcome :<?php  echo $_SESSION['user_email'];?></h4>
<ul id="menu">
<li><a href="home.php"> Home</a></li>
<li><a href="members.php"> Members</a></li>
<strong> Topics:</strong>
<?php
$get_topics="select * from topics";
$run_topics=mysqli_query($con,$get_topics);
while ($row=mysqli_fetch_array($run_topics)){
$topic_id=$row['topic_id'];
$topic_title=$row['topic_title'];
echo "<li><a href= 'topic.php?topic=$topic_id '>$topic_title </a> </li>" ;
	}
?>
<form action="results.php" method="get" id="form1" enctype="multipart/form-data">
<input type="text" name="user_query" placeholder="search a topic" required="required" />
<input type="submit" name ="search" value="Search" />
</form>
</ul>
</div>
<div id= "container1">
<div id="userss">
<h3 style="background:#4CAF50; color:#FFFFFF; height:40px; width:200px; margin-bottom:13px;margin-top:15px; padding-top:5px;"  align="center"> All Registered Users :</h3>


<?php
getUsers();
?>
</div>
<div class="content">
<div id="user_timeline">
<div id="user_details"> 
 
 <?php
 $user= $_SESSION['user_email'];
 $get_user= "select * from users where user_email='$user' ";
 $run_user= mysqli_query($con,$get_user);
 $row=mysqli_fetch_array($run_user);
 
 $user_id= $row['user_id'];
 $user_name= $row['user_name'];
 $user_image= $row['user_image'];
 $user_county= $row['user_county'];
 $user_pass= $row['user_pass'];
 $user_email= $row['user_email'];
 $user_gender= $row['user_gender'];
 $user_phone= $row['user_phone'];
 $last_login= $row['last_login'];

 
 $user_posts="select * from posts where user_id='$user_id'";
 $run_posts= mysqli_query($con,$user_posts);
 $posts= mysqli_num_rows($run_posts); 
 
 // getting the number of unread messages
 
 $select_msg ="select * from messages  where receiver = '$user_id' AND status='unread' ORDER BY 1 DESC";
$run_msg =mysqli_query($con,$select_msg);
$count_msg= mysqli_num_rows($run_msg);
 
echo " 
<img src='images/$user_image' width='200' height='200'  />
<p><strong> Name: </strong> $user_name </p>
<p><strong> Cellphone: </strong> $user_phone </p>
<p><strong> County: </strong> $user_county </p>
<p><strong> Last Login: </strong> $last_login </p>
<p> <a href='messages.php?inbox&u_id=$user_id'> My messages ($count_msg)</a></p>
<p> <a href='my_posts.php?u_id=$user_id'> My Posts ($posts)</a></p>
<p> <a href='edit.php?u_id=$user_id'> Edit Profile</a></p>
<p> <a href='logout.php'> Logout</a></p>
";
 
 ?>
 </div>
 
 </div>

<div id="content_timeline"> 
<form method="post" action="" id="f" enctype="multipart/form-data">
<table width="600" border="1" align="center">
<tbody>
<tr align="center">
<td colspan="6"> <legend>

<h3 style="background:#4CAF50; color:#FFFFFF; padding:10px; height:40px; width:620px; margin-bottom:5px; "  align="center"> Edit Your Profile Data:</h3>
</legend></td></tr>

 <tr>
  <td  style='padding:10px;' align="right">&nbsp; Full Names: </td>
   <td style='margin-top:10px; padding:10px;'>&nbsp; <input type="text" name="u_name"  value="<?php echo $user_name;?>" ></td>
   </tr>
   <tr >
   <td align="right" >&nbsp; Email Address: </td>
   <td style='margin-top:10px; padding:10px;'>&nbsp; <input type="email" name="u_email" value="<?php echo $user_email;?>" ></td>
   </tr>
   <tr>
   <td align="right">&nbsp;  Password :</td>
   <td style='margin-top:10px; padding:10px;'>&nbsp; <input type="password" name="u_pass" value="<?php echo $user_pass;?>" required="required" ></td>
   </tr>
   <tr> <td align="right">&nbsp;Phone Number :  </td>
   <td style='margin-top:10px; padding:10px;'>&nbsp; <input type="text" name="u_phone" value="<?php echo $user_phone;?>"  required="required"></td></tr>
   
 <tr >
<td align="right" >&nbsp; Photo: </td>
<td style='margin-top:10px; padding:10px;'>&nbsp; 
<input type="file" required="required" name="u_image" value="<?php echo $user_image;?>"></td>
   </tr>   
   <tr>
    <td style='padding:10px;' align="right" >&nbsp; Sex :</td>
    <td>&nbsp; 
    <select disabled="disabled" name="u_gender" >
    <option> <?php echo $user_gender;?></option>
    <option> Male</option>
    <option> Female</option>
    <select</td>
    </tr>
     
 <tr>
<td  style='padding:10px;'align="right" >&nbsp;Location: </td>
<td style='margin-bottom:10px; padding:10px;'> 
<select disabled="disabled" name="u_county">

      <option>Kitui</option>
      <option>Nrb</option>
      <option>Msa</option>
      <option>Kajiado</option>
      <option>Machakos</option>
      <option>Kisii</option>
      <option>Embu</option>
      <option>Eldoret</option>
      <option>Meru</option>
      <option>Busia</option>
      <option>Kwale</option>
      <option>Kiambu</option>
      <option>Makueni</option>
      <option>kisumu</option>
      <option>Kitale</option>
      <option>Siaya</option>
      </select></td>
    </tr>

  <tr>
 <td style='padding:10px;' align="right" >&nbsp;Job Category  </td>
  <td>&nbsp; 
  <select  required="required" name="u_role" > 
  <option value="">......Select Category.....</option>
  <option value="Vet.Officer"> Vet.Officer</option>
  <option value ="Farmer"> Farmer</option>
  <option value ="Agronomist"> Agronomist</option>
  <option value="Agri.Engineer"> Agri.Engineer</option>
  <option value="Water.Engineer"> Water.Engineer</option>
  <option value="Laboaratoligist">Laboaratoligist</option>
  <option value="HumanResource"> HumanResource</option>
  </td>
  </select>
  </tr>
  
<tr align="center"><td colspan="6" ><input type="submit" value="Update" name="update"></td></tr>
</tbody></table>
</form>
<?php
 if(isset($_POST['update'])){
	$u_name= $_POST['u_name'];
	$u_email=$_POST['u_email'];
	$u_pass=$_POST['u_pass'];
	$u_phone=$_POST['u_phone'];
	$u_image=$_FILES['u_image']['name'];
	$image_tmp=$_FILES['u_image']['tmp_name'];
	
	move_uploaded_file($image_tmp,"images/$u_image");
	
	
	
	$update = "update users set user_name ='$u_name',user_email ='$u_email',user_pass ='$u_pass',user_phone ='$u_phone',user_image ='$u_image'   where user_id='$user_id' ";
	
	$run =mysqli_query($con,$update);
	
	if($run){
	 echo "<script>alert('Your Profile Updated sucessfully')</script>";	
	 echo "<script>window.open('home.php','_self')</script>";	
		
		}
	   
	
 }
  ?> 
  
</div>
</div>
</div>
</div>
</div>
</body>
</body>
</html>

<?php  } ?>