<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Farmers ChatBox</title>
<link href="../styles/css/bootstrap.css" rel="stylesheet" />
<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet"/>
    <!--external css-->
<link href="../styles/css/zabuto_calendar.css" rel="stylesheet"/>
<link rel="stylesheet"/>
<link href="../styles/js/gritter/css/jquery.gritter.css" rel="stylesheet"/>
<link href="../styles/lineicons/style.css"" rel="stylesheet"/>
        
<link rel="stylesheet" href="../styles/style.css" media="all"/>
</head>
<body>
<div class="container">
<div id ="head_wrap">
<div id="header">
<img style="float:left;"/>
<form method="post" action="" id="form1">

<strong> Username:</strong>
<input type="text" class="form-control" name ="email" id="inputSuccess" placeholder="Enter Your Email" autofocus id="name2" required="required"/>

<strong> Password:</strong>
<input type="password" name ="pass" placeholder="********" required="required" />
<button name="login"> Login</button> 
</form>
</div>
</div>
</div>
<script src="assets/js/jquery.js"></script>
    <script src="assets/js/jquery-1.8.3.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="assets/js/jquery.sparkline.js"></script>


    <!--common script for all pages-->
    <script src="assets/js/common-scripts.js"></script>
    
    <script type="text/javascript" src="assets/js/gritter/js/jquery.gritter.js"></script>
    <script type="text/javascript" src="assets/js/gritter-conf.js"></script>

    <!--script for this page-->
    <script src="assets/js/sparkline-chart.js"></script>    
	<script src="assets/js/zabuto_calendar.js"></script>
</body>
</html>