
<?php 
session_start(); 
include("../functions/functions.php");
if(!isset($_SESSION['admin_email'])){
	header("location:admin_login.php");
	}
	else
	{
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" href="style.css">
<link rel="shortcut icon" href="../images/favicon.png">
<title>Welcome to Admin Panel</title>
</head>
<body>
<div class ="containter">
<div id ="head">
<h2>Admin Panel</h2>

</div>

<div id ="content">


<?php
if(isset($_GET['view_users'])){
	include("includes/view_users.php");
}
if(isset($_GET['view_posts'])){
	include("includes/view_posts.php");
}

if(isset($_GET['view_comments'])){
	include("includes/view_comments.php");
}

?>


</div>
<div id ="sidebar">
<h3>Manage Content:</h3>
<ul id="menu">
<li> <a href="index.php?view_users">View Users</a></li>
<li> <a href="index.php?view_posts">View Posts</a></li>
<li> <a href="index.php?view_comments">View Comments</a></li>
<li> <a href="admin_logout.php">Admin logout</a></li>
</ul>
</div>
<div id ="foot">
<h4>copyrights @ 2017 || www.demasua.com</h4>
</div>
</div>
</body>
</html>
<?php } ?>